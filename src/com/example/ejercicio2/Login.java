package com.example.ejercicio2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity implements OnClickListener {
	Button btnLogin;
	EditText txtUsuario, txtPassword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		btnLogin = (Button)findViewById(R.id.btnIniciar);
		
		txtUsuario = (EditText)findViewById(R.id.txtUsuario);
		txtPassword = (EditText)findViewById(R.id.txtPassword);
		
		btnLogin.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnIniciar:
			login();
			break;

		default:
			break;
		}
	}

	private void login() {
		String usuario = txtUsuario.getText().toString();
		String password = txtPassword.getText().toString();
		
		if(usuario.length()==0 || password.length()==0)
		{
			Toast.makeText(this,"Usuario o password no pueden ser vacios" , Toast.LENGTH_LONG).show();
		}
		else
		if(usuario.compareToIgnoreCase("desarrolloweb") != 0  || password.compareTo("#dwAndroid") != 0 )
		{
			Toast.makeText(this,"Usuario o password incorrectos" , Toast.LENGTH_LONG).show();
		}
		else
		{
			Intent bienvenida = new Intent(this,Bienvenida.class);
			bienvenida.putExtra("usuario", usuario);
			startActivity(bienvenida);
		}
		
		
		
	}

}
