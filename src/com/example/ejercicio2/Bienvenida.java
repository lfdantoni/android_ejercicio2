package com.example.ejercicio2;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Bienvenida extends Activity {
	TextView tvUsuario;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bienvenida);
		
		tvUsuario = (TextView)findViewById(R.id.tvUsuario);
		
		Bundle login = getIntent().getExtras();
		
		tvUsuario.setText("Bienvenido " + login.getCharSequence("usuario").toString());

	}
}
